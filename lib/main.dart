import 'package:flutter/material.dart';

/**
 * EAG202 – Desenvolvimento de Aplicações para Dispositivos Móveis (HÍBRIDOS) - 2021.1
 * Duplas: Karen Santos e Nelci Gomes
 */

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculadora de IMC',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: CalculadoraIMC(),
    );
  }
}

class CalculadoraIMC extends StatefulWidget {
  @override
  _CalculadoraIMCState createState() => _CalculadoraIMCState();
}

class _CalculadoraIMCState extends State<CalculadoraIMC> {

  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _info = "Informe seus dados";
  double imc;
  double resultIMC;
  String classificacao;
  Color colorResult;
  
  void _resetFields(){
    weightController.text = "";
    heightController.text = "";

    setState(() {
      _info = "";
      resultIMC = null;
      _formKey = GlobalKey<FormState>();
    });
  }


  void _calculateIMC (){
    setState(() {
      double weight= double.parse(weightController.text);
      double height= double.parse(heightController.text) / 100;

      double imc = weight / (height * height);

      resultIMC = imc;

      print("IMC = (${imc.toStringAsPrecision(2)})");

      if (imc < 18.6 ){
        _info = "Abaixo do peso ";
        colorResult = Colors.blue[200];
      } else if(imc >= 18.6 && imc < 24.9){
        _info = "Peso ideal";
        colorResult = Colors.green[200];
      } else if(imc >= 24.9 && imc < 29.9){
        _info = "Levemente acima do peso";
        colorResult = Colors.yellow[200];
      } else if(imc >= 29.9 && imc < 34.9){
        _info = "Obesidade grau I";
        colorResult = Colors.orange[200];
      } else if(imc >= 34.0 && imc < 39.9){
        _info = "Obesidade grau II";
        colorResult = Colors.red[200];
      } else if(imc >40){
        _info = "Obesidade grau III";
        colorResult = Colors.purple[200];
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculadora de IMC"),
        centerTitle: true,
        backgroundColor: Colors.deepOrangeAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _resetFields,
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children:[
            resultIMC == null || resultIMC == '' ? 
            Text('Digite os valores abaixo') :
            Container(
              width: 300,
              height: 300,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(150),
                border: Border.all(
                  width: 10,
                  color: colorResult
                )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(resultIMC != null ? resultIMC.toStringAsPrecision(2) : '',
                    style: TextStyle(
                      fontSize: 48,
                      color: colorResult
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text( _info,
                    style: TextStyle(
                      fontSize: 24,
                      color: colorResult
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text('Seu peso'),
                    Container(
                      width:100,
                      child: TextField(
                        controller: weightController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20)
                          ),
                          suffixText: 'kg'
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width:20
                ),
                Column(
                  children: [
                    Text('Sua altura',),
                    Container(
                      width:100,
                      child: TextField(
                        controller: heightController ,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20)
                          ),
                          suffixText: 'cm'
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
                padding: EdgeInsets.only(top: 15, bottom: 10),
                child: Container(
                  height: 50,
                  child: RaisedButton(
                    onPressed: () {
                      _calculateIMC();
                    },
                    child: Text(
                      "Calcular",
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                    color: Colors.deepOrangeAccent,
                  ),
                ),
              ),
          ] 
        ),
      ),
    );
  }
}